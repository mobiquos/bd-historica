# README #

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Script que realizan DUMP de la base de datos

Script Dump_full

En este script se realiza un dump full de las tablas que no contienen un campo "Fecha"
Para ejecutarlo solo basta con realizar el siguiente comando desde una terminal linux.

sh dump_full.sh 

Este script, lee el archivo llamado "tablas_resp_full", el cual contiene el listado de tablas en las que se realizará el respaldo FULL, ya que no contienen un campo de tipo fecha.

----------------------------------------------------------------------------------------------------------------------------------------

Script dump_condicionado.sh 

Este script realiza un dump condicionado de una tabla en particular, en donde se deben enviar por parametros, el nombre de la tabla, el nombre del campo fecha y los rangos de fecha deseados
Para ejecutar el script se debe escribir lo siguiente desde una terminal.

sh dump_condicionado.sh [nombre_tabla] [campo_fecha] [año_inicio] [año_termino]

Ejemplo:

sh dump_condicionado.sh animal fecha 2009 2015


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPTS PARA CONTEO DE REGISTROS

SCRIPT CONTAR

Este script realiza un conteo de una tabla, la cual es enviada por parametros desde una terminal Linux, para ello 
se debe ejecutar el siguiente comando

sh contar.sh [nombre_tabla]

Ejemplo:

sh contar.sh animal

resultado:
count(*)
1085668

----------------------------------------------------------------------------------------------------------------------------------------

SCRIPT FULL_COUNT

En este script se guardan los numeros de registros de todas las tablas del sistema de produccion SPC y para que funcione solo basta con realizar lo siguiente.

sh full_count.sh


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


SCRIPT DE CONSULTA

SCRIPT CONSULTA

Este Script se utiliza para realizar consultas a la base de datos de SPC-Producción, sin la necesidad de autentificarse, para su ejecución se debe realizar lo siguiente:

sh consulta.sh "[consulta_sql]"  #la consulta debe estar entre comillas dobles

Ejemplo:

sh consulta.sh "SELECT * FROM ANIMAL"


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT DE CARGA

Este script realiza la operación de carga de datos a tráves de un archivo sql enviado por parametros. El cual debe ejecutarse de la siguiente manera.


sh carga.sh  [nombre_archivo].sql