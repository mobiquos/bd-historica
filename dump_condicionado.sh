#!/bin/sh
########################################################################
# dump_condicionado.sh 
# Este script realiza un dump condicionado de una tabla en particular, en donde se deben enviar por parametros, el nombre de la tabla, el nombre del campo fecha y los rangos de fecha deseados
# Copyright (C) Juan Pablo Muena Q - jmuenaq@mobiquos.cl - Mobiquos LTDA.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
######################################################################## 

# Se guardan los parametros de conexion en variables

host="192.168.0.4"
usuario="produccion"
password="8472"
db="produccion"

# se ejecuta el comando dump

mysqldump --no-create-info --database $db -h "$host" -u"" $usuario"" --password="$password" --tables $1 --where "STR_TO_DATE($2,'%Y') > STR_TO_DATE('$3','%Y') AND STR_TO_DATE($2,'%Y') < STR_TO_DATE('$4','%Y')" > dump/dump_$1_$(date +%d-%m-%Y).sql

