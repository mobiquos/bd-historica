#!/bin/sh
########################################################################
# carga.sh 
# Este script realiza la operación de carga de datos a tráves de un archivo sql enviado por parametros.
# Copyright (C) Juan Pablo Muena Q - jmuenaq@mobiquos.cl - Mobiquos LTDA.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
######################################################################## 

# Se guardan los parametros de conexion en variables

host="172.16.1.89"  # cambiar dirección a base de datos historica 192.168.0.83
usuario="produccion"
password="8472"
db="produccion"

#Se guarda en la variable comando, la lista de instrucciones
comando="-h "$host" -u"" $usuario"" -p"$password" -D$db "

# Se ejecuta el comando, al momento de ejecutar el script, en donde $1 es el nombre del archivo, enviado por parametros al momento de su ejecución

mysql -h "$host" -u"" $usuario"" -p"$password" -D$db < $1
