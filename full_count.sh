#!/bin/sh
########################################################################
# full_count.sh 
# Se guardan los numeros de registros de todas las tablas del sistema de produccion SPC
# Copyright (C) Juan Pablo Muena Q - jmuenaq@mobiquos.cl - Mobiquos LTDA.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
######################################################################## 


mysql -h 192.168.0.4 -u produccion -p8472 -Dproduccion -s -e "SELECT TABLE_NAME, TABLE_ROWS   FROM information_schema.tables t WHERE table_schema = 'produccion' ORDER BY TABLE_ROWS DESC;"




