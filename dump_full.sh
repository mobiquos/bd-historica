#!/bin/sh
########################################################################
# dump_full.sh 
# En este script se realiza un dump full de las tablas que no contienen un campo "Fecha"
# Copyright (C) Juan Pablo Muena Q - jmuenaq@mobiquos.cl - Mobiquos LTDA.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
######################################################################## 


# Se guardan los parametros de conexion en variables

host="192.168.0.4"
usuario="produccion"
password="8472"
db="produccion"

#Se guarda en la variable comando, la lista de instrucciones


while read tabla
do
parametros_dump="--no-create-info --database $db -h "$host" -u"" $usuario"" --password="$password" --tables $tabla"

#Se ejecuta la SQL  enviado por parametros
mysqldump $parametros_dump


done < tablas_resp_full  



